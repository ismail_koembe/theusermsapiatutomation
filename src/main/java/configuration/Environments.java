package configuration;

/**
 * @author Ismail Koembe
 */
public enum Environments {
    PRODUCTION("production"),
    STAGE("stage"),

    LOCAL("local");

    public final String name;

    Environments(String name) {
        this.name = name;
    }
}
