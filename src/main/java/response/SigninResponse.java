package response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * @author Ismail Koembe
 */
@Data
public class SigninResponse {
    private String token;
    private String type;
    private String id;
    private String accountId;
    private String photoUrl;
    private String firstname;
    private String lastname;
    @JsonIgnore
    private String middleName;

    private String [] userRole;

}
