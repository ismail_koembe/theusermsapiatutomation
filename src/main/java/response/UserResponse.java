package response;

import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @author Ismail Koembe
 */
@Data
public class UserResponse {
    private String accountId;
    private String firstname;
    private String lastname;
    private boolean isGuardianRequired;
    private List<Major> majorList;
    private boolean isTemporarilyPassword;
    private Set<Roles> roles;


    @Data
    public static class Major {
        private String name;
        private Integer code;
        private boolean isPrimary;
    }

    public enum Roles {
        ROLE_ADMIN ,
        ROLE_STUDENT,
        ROLE_GUARDIAN,
        ROLE_TEACHER;
        private String name;

        public String getName() {
            return name;
        }
    }
}
