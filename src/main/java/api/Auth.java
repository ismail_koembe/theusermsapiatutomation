package api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.JsonObject;
import configuration.PropManager;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import lombok.Data;
import response.SigninResponse;

import static io.restassured.RestAssured.given;

/**
 * @author Ismail Koembe
 */
@Data
public class Auth {
    public static String getBaseAPI(String environment) {
        return PropManager.getProperties(environment, "baseUrl");
    }
    protected static RequestSpecification addHeaders(String env) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader("accept", "application/json")
                .addHeader("Content-Type", "application/json");
        return builder.build();
    }


    public static SigninResponse signin(String env, String username, String password){
        RequestSpecification request = addHeaders(env);

        JsonObject body = new JsonObject();
        body.addProperty("username", PropManager.getProperties(env, username));
        body.addProperty("password", PropManager.getProperties(env, password));

        return given()
                .spec(request)
//                .log().all()
                .when()
                .body(body.toString())
                .post(getBaseAPI(env) + "/api/auth/signin")
                .then()
                .extract().as(SigninResponse.class);


    }
}
