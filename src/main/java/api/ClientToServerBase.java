package api;


import configuration.PropManager;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;


/**
 * @author Ismail Koembe
 */
public class ClientToServerBase {
    public static String getBaseAPI(String environment) {
        return PropManager.getProperties(environment, "baseUrl");
    }

    protected static RequestSpecification addHeaders(String env, String token ) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader("accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer " + token);
        return builder.build();
    }
}
