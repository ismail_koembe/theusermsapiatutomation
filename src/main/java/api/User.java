package api;

import com.google.gson.JsonObject;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import response.UserResponse;

import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * @author Ismail Koembe
 */
public class User extends ClientToServerBase{
        public static List<UserResponse> getUserByGender(String environment, String token){
            RequestSpecification request = addHeaders(environment, token);

            return given()
                    .spec(request)
//                    .log().all()
                    .when()
                    .header("gender","FEMALE")
                    .get(getBaseAPI(environment) + "/api/user/byGender")
                    .then()
                    .extract().response().as(new TypeRef<List<UserResponse>>() {});
        }
}
