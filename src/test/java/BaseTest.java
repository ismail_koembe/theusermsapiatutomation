import configuration.Environments;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.lang.reflect.Method;

/**
 * @author Ismail Koembe
 */
@Slf4j
public class BaseTest {
    public String env = Environments.STAGE.name;

    @BeforeMethod
    public void setupUserContext(@Optional("environment") String environment,
                                 @Optional("username") String username,
                                 @Optional("password") String password,Method method) {
        /**
         * BaseTest class is essential part of the framework.
         * In order to switch environment, BaseTest class holds central point.
         */
        log.info("Running tests on: " + env);
        log.info("Running test Method: " + method.getName());
    }


    @Parameters({"environment", "username", "password"})
    @AfterSuite
    public void init(@Optional("environment") String environment,
                     @Optional("username") String username,
                     @Optional("password") String password) {
        /**
         * Not implemented just for demo purpose for interview
         * When the Jenkins triggers framework or QA person run via terminal or xml file
         * those injected parameters will be initialized.
         * enabled = false because it is not implemented
         * */

    }
}
