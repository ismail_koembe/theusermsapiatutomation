import api.Auth;
import api.User;
import configuration.Environments;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import response.SigninResponse;
import response.UserResponse;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ismail Koembe
 */
public class UserControllerTest extends BaseTest {
   @Test
    public void getUserByGender(){
       SigninResponse signin = Auth.signin(Environments.STAGE.name, "username", "password");
       List<UserResponse> userByGender = User.getUserByGender(env, signin.getToken());
       List<UserResponse> guardianRequiredUsers = userByGender.stream().filter(UserResponse::isGuardianRequired)
               .collect(Collectors.toList());
       Assert.assertTrue(guardianRequiredUsers.size() > 0, "At least 1 guardian required user is expected ");

   }
}
